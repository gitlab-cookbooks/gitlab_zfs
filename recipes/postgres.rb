# Cookbook Name:: gitlab_zfs
# Recipe:: postgres
# License:: MIT
#
# Copyright 2019, GitLab Inc.

postgres_user   = node['gitlab_zfs']['postgres']['user']
postgres_group  = node['gitlab_zfs']['postgres']['group']
tank_mountpoint = node['gitlab_zfs']['postgres']['tank_mountpoint']

user postgres_user do
  manage_home false
  not_if { node['etc']['passwd'].key?(postgres_user) }
end

zfs 'tank/postgresql' do
  action :create
end

%w[base pg_xlog].each do |dataset|
  mountpoint = "#{tank_mountpoint}/postgresql/data/#{dataset}"

  zfs "tank/postgresql/#{dataset}" do
    properties [
      { recordsize: '8k' },
      { redundant_metadata: 'most' },
      { primarycache: 'metadata' },
      { logbias: 'throughput' },
      { mountpoint: mountpoint }
    ]
  end

  directory mountpoint do
    group postgres_group
    user postgres_user
  end
end
