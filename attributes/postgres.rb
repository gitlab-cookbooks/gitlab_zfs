default['gitlab_zfs']['postgres']['user'] = 'gitlab-psql'
default['gitlab_zfs']['postgres']['group'] = 'gitlab-psql'
default['gitlab_zfs']['postgres']['tank_mountpoint'] = '/var/opt/gitlab'
