name             'gitlab_zfs'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Cookbook for managing ZFS datasets on GitLab infrastructure'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'
chef_version     '>= 12.1' if respond_to?(:chef_version)
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_zfs/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_zfs'

supports 'ubuntu', '= 18.04'

depends 'chef_zfs', '~> 1.0.14'
